function map(element, cb) {
    if (Array.isArray(element)) {
        const arr = []

        for (let numbers of element) {
            arr.push(cb(numbers));
        }
        return arr;
    }
}


module.exports = map;