function reduce(elements,cb, startingValue) {

  let index = 0
  if (typeof (startingValue) == "undefined") {
    index = 1
    startingValue = elements[0] 
  }

  if (Array.isArray(elements) && elements.length != 0) {

    for (index; index < elements.length; index++) {
      startingValue = cb(startingValue, elements[index], index, elements);

    }
    return startingValue
  }
};

module.exports = reduce