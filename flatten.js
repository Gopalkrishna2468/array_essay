function flatten(elements) {
  let arr = [];

  function removeArray(item) {
    Array.isArray(item) ? checkItems(item) : arr.push(item);
  }

  function checkItems(array) {
    for (let num of array) {
      console.log(num);
      removeArray(num);
    }
  }
  checkItems(elements);
  return arr;
}

module.exports = flatten;