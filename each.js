function each(elements, cb) {
    for (let numbers of elements) {
        cb(numbers);
    }
}

module.exports = each;