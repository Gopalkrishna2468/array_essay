function find(items, cb) {
   let element = undefined;
   for (let numbers of items) {
      if (cb(numbers)) {
         return element = numbers;
      }
   }
   return element;
}

module.exports = find;